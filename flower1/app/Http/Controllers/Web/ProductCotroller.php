<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductCotroller extends Controller
{
    public function detail_Product($id)
    {
        // dd($id);
        // $products= Product::all();
        // $products = DB::table('products')->where('id',$id)->get();
        // dd($products);
        $products= Product::findOrFail($id);
        return view('frontend.product.details', compact('products'));
    }
    // public function addCart($id)
    // {
    //     $products= Product::findOrFail($id);
    //     $cart = session()->get('cart', []);
 
    //     if(isset($cart[$id])) {
    //         $cart[$id]['quantity']++;
    //     }  else 
    //     {
    //         $cart[$id]= [
    //             'name' => $products->name,
    //             'img'=>$products->img,
    //             'price' => $products->price,
    //             'quantity' => 1
    //         ];
    //     }
       
    //     session()->put('cart', $cart);
    //     return redirect()->back()->with('success', 'Product add to cart successfully!');
    // }

    
    public function flower_wedding(Product $products)
    {
        
        // dd($categories);
        $products=Product::whereHas('category', function ($query) {
            return $query->where('desc', 'flower-wedding');
        })->limit(8)->with('inventory')->orderBy('id','desc')->get();
        // dd($products);
        $productWithCategories= Product::with('category')->get();
        return view('frontend.index', compact('products','productWithCategories' ));
    }
    public function tet_flower(Request $request)
    {

            $products=Product::whereHas('category', function ($query) {
                return $query->where('name_category', 'Hoa Tet 2023');
            })->limit(16)->with('inventory')->orderBy('id','desc')->get();

  
        return view('frontend.index',compact('products'));

    }
    public function flower_birthday()
    {
        $products=Product::whereHas('category', function ($query) {
            return $query->where('desc', 'flower-birthday');
        })->limit(16)->with('inventory')->orderBy('id','desc')->get();

        return view('frontend.index',compact('products'));
    }
    public function flower_opeing()
    {
        $products=Product::whereHas('category', function ($query) {
            return $query->where('desc', 'flower-opeing');
        })->limit(16)->with('inventory')->orderBy('id','desc')->get();

        return view('frontend.index',compact('products'));
    }

}


