<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Models\User_address;
use App\Models\UserAddress;
use GuzzleHttp\Promise\Create;
use Illuminate\Auth\Events\Logout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class AuthenticateController extends Controller
{
    public function showIndex(Product $products)
    {
       $products = Product::limit(20)->orderBy('id','desc')->get();
       
    //    $products = Product::find($id);
        // dd($products);
        return view('frontend.index', compact('products'));
    }
    public function showLogin()
    {
        
        return view('frontend.authenticate.login');

    }
    public function login(Request $request)
    {
        // dd($request->input());
        $email = $request->input('email');
        $password = $request->input('password');
        (Auth::attempt([
            'email' => $email,
            'password' => $password,
            'role' => '3' 
        ]));
        return redirect("/");
    }
    public function showRegisterForm()
    {
        return view('frontend.authenticate.register');
    }
    public function register(Request $request)
    {
        // dd($request->input());
        // dd(User::with('user_address')->get());
        $request->validate([
            'user_name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role' => 'required',
            'address' =>'required',
            'city' =>'required',
            'country' =>'required',
            'password' => 'required|confirmed'
        ]);
        // dd($request->input());
        // $user= User::create($request->input()); // cap nhap vao bang user;

        $userData = array_merge($request->except(['address','city','country','password_confirmation'])) ;
        $user = User::create($userData);
        // dd($user->id);
        $userAddressData = array_merge($request->only(['city','address','country']),['user_id'=> $user->id]);
        UserAddress::create($userAddressData);
        if($user){
            Auth::login($user);//login
            return redirect('');
        }
        

    }
    public function logout(){
       
        Auth::logout();
        return redirect('');
    }

}
