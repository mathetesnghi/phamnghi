<?php

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input\Input;

class CartController extends Controller
{
    public function AddCart(Request $request, $id)
    {
        $product = DB::table('products')->where('id', $id)->first();
        // $product = Product::find($id);

        if($product != null){
            $oldCart = session('Cart') ? session('Cart') : null;
            
            $newCart = new Cart($oldCart);
            $newCart->AddCart($product, $id);
            // $request->session()->put('Cart',$newCart);
            session(['Cart' => $newCart]);
            // dd($newCart);

        }
        return view('frontend.Cart.cart');
        // $product = Product::find($id);
        // if($product != null)
        // {
        //     $oldCart = session('Cart') ? session('Cart') : null;
        //     $newCart = new Cart($oldCart);
        //     $newCart->AddCart($product, $id);
        //     $request->session()->put('Cart', $newCart);
        //     dd($newCart);
        // }
    }
    public function DeleteItemCart(Request $request , $id)
    { 
        $oldCart = session('Cart');
        $newCart = new Cart($oldCart);
        $newCart->DeleteItemCart($id);
        if(count($newCart->products) > 0){
            session(['Cart' => $newCart]);

        }
        else{
            Session::forget('Cart');
        }
        return view('frontend.Cart.cart');
    }
    public function showShoping()

    {
        // $product = DB::table('products')->where('id', $id)->first();
        
        // if($product != null){
        //     $oldCart = session('Cart') ? session('Cart') : null;
            
        //     $newCart = new Cart($oldCart);
        //     $newCart->AddCart($product, $id);
        //     // $request->session()->put('Cart',$newCart);
        //     session(['Cart' => $newCart]);
        //     // dd($newCart);
        // }
        return view('frontend.Cart.list');
    }
    public function DeleteListItemCart(Request $request , $id)
    { 
        $oldCart = session('Cart');
        $newCart = new Cart($oldCart);
        $newCart->DeleteItemCart($id);
        if(count($newCart->products) > 0){
            session(['Cart' => $newCart]);

        }
        else{
            Session::forget('Cart');
        }
        return view('frontend.Cart.list-cart');
    }
    public function SaveListItemCart(Request $request , $id,$quantity)
    { 
        $oldCart = session('Cart');
        $newCart = new Cart($oldCart);
        $newCart->UpdateItemCart($id,$quantity);
        session(['Cart' => $newCart]);
        return view('frontend.Cart.list-cart');
    }


    public function showOrderDetails( )
    {
        $orderData = [
            'total_price' => session('Cart')->totalPrice,
            'full_name' => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            'address' => Auth::user()->userAddress->address .', '.Auth::user()->userAddress->city.','. Auth::user()->userAddress->country,
            'telephone' => Auth::user()->telephone, 
            'email' => Auth::user()->email
        ];
        
        return view('frontend.Cart.showOrderDetails', compact('orderData') );
    }
    public function updateOrder(Request $request)
    {
        // $orderData =[
        //     'total_price' => session('Cart')->totalPrice,
        //     'full_name' => Auth::user()->first_name . ' ' . Auth::user()->last_name,
        //     'telephone' => Auth::user()->telephone, 
        //     'email' => Auth::user()->email
        // ];
        // Order::created($orderData);
        // dd( $orderData);
        
    }
}
