<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    
    public function account()
    {
        $accounts = User::all();
        return view('backend.account.admin', compact('accounts'));
    }

    public function delete($id)
    {
        User::destroy($id);
        return redirect('admin/search/admin');
    }
    public function adminInex()
    {
        $accounts= DB::table('users')->where('role', 1)->paginate(10);
        return view('backend.account.admin',compact('accounts'));
    }
    public function clientInex()
    {
        $accounts= DB::table('users')->where('role', 3)->paginate(10);
        return view('backend.account.admin',compact('accounts'));
    }
    public function editAccount($id)
    {
        $accounts = User::find($id);
        // dd($accounts);
        return view('backend.account.editAccount',compact('accounts'));
    }
    public function update(Request $request, $id)
    {
        $accounts= User::find($id);
    //    dd($accounts);
        $accounts->update($request->all());
        return redirect('admin/search/client');
    }

}
