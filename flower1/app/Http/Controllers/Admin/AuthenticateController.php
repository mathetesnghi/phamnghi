<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateController extends Controller
{
    public function showLoginAdmin()
    {
        return view('backend.authenticate.login');
    }
    public function loginAdmin(Request $request)
    {
        // dd($request->input());
        $email = $request->input('email');
        $password = $request->input('password');
        if(Auth::attempt([
            'email' => $email,
            'password' => $password,
            'role' => 1 
        ])){
            return view('backend.authenticate.layouts.app');
        }

        return view('errors.503');
        
    }
    //1 để dùng đăng nhập trực tiếp bên ngoài nhưng bị chặng bowrr middleware
    public function index()
    {
        return view('backend.authenticate.layouts.app');
    }
    public function logout(){
        Auth::logout();
        return redirect('admin');
    }
    // public function showNav()
    // {
    //     return view('backend.authenticate.layouts.app');
    // }




}

