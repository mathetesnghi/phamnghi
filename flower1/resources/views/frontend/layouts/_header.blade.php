<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<div class="header">
    <div class="container">
        <div class="row " id="head">
            <div class="col-md-2">
                <div class="icons">
                    <a href="facebook.com"><i class="fa-brands fa-facebook"></i></a>
                    <a href="#!"> <i class="fa-brands fa-instagram"></i></a>
                    <a href="https://mail.google.com/mail/u/0/#inbox"> <i class="fa-sharp fa-solid fa-envelope"></i></a>
                    <a href="#!"><i class="fa-solid fa-phone"></i></a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="information">
                    <h6 data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nangnghiofficial@gmail.com"><i
                            class="fa-sharp fa-solid fa-envelopes-bulk"></i>Nangnghiofficial@gmail.com</h6>
                    <h6 data-bs-toggle="tooltip" data-bs-placement="bottom" title="OPEN => 07:00-22:00" class="clock">
                        <i class="fa-regular fa-clock"></i>07:00-22:00
                    </h6>
                    <h6 data-bs-toggle="tooltip" data-bs-placement="bottom" title="Phone number: 033578444"
                        class="phone"><i class="fa-solid fa-phone"></i>0335784744</h6>


                    {{-- data-bs-toggle="tooltip" data-bs-placement="bottom" title="Adress: 190 Ngõ Quỳnh, Thanh Nhàn, Hà Nội" --}}
                    <div class="map">
                        <!-- Button trigger modal -->
                        <button type="button" class="adress" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                            <i data-bs-toggle="tooltip" data-bs-placement="bottom"
                                title="Adress: 190 Ngõ Quỳnh Thanh Nhàn Hà Nội" class="fa-solid fa-location-dot"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
                            tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="staticBackdropLabel">190 Ngõ Quỳnh,Thanh Nhàn,
                                            Hà Nôi </h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7863400456704!2d105.85327411488286!3d21.00120008601331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac0b8001b895%3A0x994ddbbc99d974da!2zMTkwIE5nLiBRdeG7s25oLCBRdeG7s25oIEzDtGksIEhhaSBCw6AgVHLGsG5nLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1673537483965!5m2!1svi!2s"
                                            width="600" height="450" style="border:0;" allowfullscreen=""
                                            loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="login_cart">
                    <div class="login">

                        @if (Auth::check())
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                    role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa-sharp fa-solid fa-unlock-keyhole"></i>{{ Auth::user()->user_name }}
                                </a>
                                <ul class="dropdown-menu p-0" style="width: 0cm; height:1.5cm  "
                                    aria-labelledby="navbarDropdownMenuLink">
                                    <li class="p-0"><a class="dropdown-item p-0" href="#">Trang Ca Nhan</a>
                                    </li>
                                    <li class="p-0"><a class="dropdown-item p-0" href="{{ url('logout') }}">Dang
                                            Xuat</a></li>

                                </ul>
                                {{-- <div class="toast show">
                    <div class="toast-header">
                      Toast Header
                      <button type="button" class="btn-close" data-bs-dismiss="toast"></button>
                    </div>
                    <div class="toast-body">
                      Some text inside the toast body
                    </div>
                  </div> --}}
                            @else
                                <!-- Button trigger modal -->
                                <button type="button" class="btn_login"
                                    data-bs-toggle="modal"data-bs-target="#exampleModal">
                            <li><i class="fa-regular fa-user"></i>Đăng Nhập</li>
                            </button>
                        @endif
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" id="form_login">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Đăng Nhập</h1>
                                        <button type="button" class="btn-close" id="exit"
                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body" id="background_login">

                                        {{-- LOGIN --}}

                                        <form method="POST" action="/login">
                                            @csrf
                                            <div class="mb-3">
                                                <label for="exampleInputEmail1" class="form-label">Email
                                                    address</label>
                                                <input type="email" name="email" class="form-control"
                                                    id="exampleInputEmail1" aria-describedby="emailHelp">

                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                                <input type="password" name="password" class="form-control"
                                                    id="exampleInputPassword1">
                                            </div>
                                            <div class="mb-3 form-check">
                                                <input type="checkbox" class="form-check-input" name="remember_token"
                                                    id="exampleCheck1">
                                                <label class="form-check-label" for="exampleCheck1">Check me
                                                    out</label>
                                            </div>
                                            <button type="submit" class="btn btn-primary ml-3">login</button>
                                            <a style="color: rgb(165, 165, 153)" href="{{ url('/register') }}">Bạn
                                                chưa có tài khảo</a>
                                        </form>
                                        {{-- ENG LOGIN --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- Cart --}}
                    {{-- @if (Session::has('Cart') != null) --}}
                    {{-- Dropdown button --}}
                    @if (Session::has('Cart') != null)
                        <div class="dropdown">
                        <li class=" dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <a class=" cart_text position-relative"data-bs-toggle="tooltip" data-bs-placement="bottom" title="$:{{ Session('Cart')->totalPrice }}"><i class="fa-solid fa-cart-shopping"></i>
                        @else
                        <a class=" cart_text position-relative"data-bs-toggle="tooltip" data-bs-placement="bottom" ><i class="fa-solid fa-cart-shopping"></i>
                        @endif
                            @if (Session::has('Cart') != null)
                                <span id="total-quantity-show"   style="font-size:7px"class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                        {{-- {{ count(session('Cart')->products) }} --}}
                                        {{ (session::get('Cart')->totalQuantity) }}
                                </span>
                            @else
                        
                                <span id="total-quantity-show" style="font-size:7px"class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                    {{-- {{ count(session('Cart')->products) }} --}}
                                        0
                                </span>
                            @endif
                            </a>
                        </li>
                    {{-- @endif --}}
                        {{-- cart shoping --}}
                        <div id="data-cart" class=" dropdown-menu">
                            @if (Session::has('Cart') != null)
                                <div class="row">
                                    <table class="table table-dark table-striped">
                                        <thead>
                                            <tr>

                                                <th scope="col">Tên</th>
                                                <th scope="col">Ảnh</th>
                                                <th scope="col">Số lượng</th>
                                                <th scope="col">Xóa</th>
                                            </tr>
                                        </thead>
                                        @foreach (Session::get('Cart')->products as $item)
                                            {{-- @dd($item['productInfo']->img) --}}
                                            <tbody>
                                                <tr>
                                                    <td> {{ $item['productInfo']->name }}</td>
                                                    <td>
                                                        <div class="">
                                                            @foreach (json_decode(($item['productInfo']->img)) as $x => $image )
                                                            {{-- @dd($image) --}}
                                                            <div class="col-sm-2 hidden-xs">
                                                                <img src="{{ asset('storage/backend/product/'. $image) }}" style="height:80px;width:80px" alt="">
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                    <td> {{ $item['productInfo']->price }} x {{ $item['quantity'] }}
                                                    </td>
                                                    <td class="si-close">
                                                        <i class="fa-solid fa-xmark"
                                                            data-id="{{ $item['productInfo']->id }}"></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        @endforeach
                                        <table>
                                            <thead>

                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Tổng:{{ number_format(Session::get('Cart')->totalPrice) }} VNĐ
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </table>
                                </div>
                                @endif
                            </div>
                        {{-- end cart? --}}
                    </div>
                </div>
                <div>
                    {{-- <li class=" cart_text position-relative"data-bs-toggle="tooltip" data-bs-placement="bottom"
                            title="$: {{ session('Cart')->totalPrice }}"><i class="fa-solid fa-cart-shopping"></i>Giỏ
                            Hàng
                            <span style="font-size:7px"
                                class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                {{ count(session('Cart')->products) }}

                                <span class="visually-hidden">unread messages</span>
                            </span>
                        </li> --}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script>
    //bosstrap %toasts
    //tooltip
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>
