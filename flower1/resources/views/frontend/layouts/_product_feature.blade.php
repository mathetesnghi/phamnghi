@include('frontend.layouts._slide')
<div class="container1">
    {{-- @foreach ($productWithCategories as $product)

    <h3>{{ $product->category->name_category }}</h3> 

@endforeach --}}


<h3 style="text-align:center">Product</h3>
<div class="products-container">
    <div class="row">
        @foreach ($products as $product)
            <div class="col-md-3">
                <a href="{{ url("product/{$product->id}/details") }}">
                    <div class="product" data-name="p-1">
                        @foreach ($product->img as $images)
                            {{-- @dd($product) --}}
                            <img src="{{ asset('storage/backend/product/' . $images) }}"
                                style="width:180px; height:180px" />
                        @endforeach
                        <h3>{{ $product->name }}</h3>
                        <div class="price">{{ number_format($product->price) }} <span>VN</span></div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>

</div>
