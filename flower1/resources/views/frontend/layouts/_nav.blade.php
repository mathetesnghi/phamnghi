<div class="container">
    <div class="row align-items-md-center">
        
    

        <div id="logo_1" class="col-md-2 d-flex">
        <div >
            <button id="icon-menu" data-bs-toggle="offcanvas" 
            data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><i class="fa-solid fa-bars"></i></button>
            <div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
            <div class="offcanvas-header">
                <h2>Menu</h2>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="menu">
                <ul>
                    <li><a href="{{url('/')}}">Trang Chủ</a> </li>
                    <li><a href="{{url('/')}}">Giới Thiệu</a> </li>
                    <div class="btn-group">
                    <a class=" dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Danh Mục
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url("product/tet_flower") }}">Hoa Tết 2023</a> </li>
                        <li><a href="{{ url("product/flower_wedding") }}">Hoa Cưới Đẹp</a> </li>
                        <li><a href="{{ url("product/flower_birthday") }}">Hoa Sinh Nhật</a> </li>
                        <li><a href="{{ url("product/flower_opeing") }}">Hoa Khai Trương</a> </li>
                        <li><a href="{{ url("/") }}">Tat Cả </a> </li>
                    </ul>
                    </div>
                    <li><a href="">Hoa Đặt</a></li>
                    <li><a href="{{ url('product/shoping-cart') }}">Giỏ hàng</a></li>

                </ul>
        
                </div>
            </div>
            </div>
        </div>
        <div class="logo">
            <img src="{{asset('frontend/img/logo/Nghi-H1.png')}}" width="306px" alt=""> 
        </div>
        </div>
        <div class="col-md-10">
        <div class="title_text">
            <h1>"Shop Hoa NGHI PHẠM - ĐIỆN LÀ CÓ - Giao Tận NơI"</h1>
        </div>
        </div>  
    </div>  
</div>  
<div class="header-main">


    <div class="header-wrapper">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-4">

            </div>
            <div class="col-md-8">
            <div class="menu">
                <ul>
                    <li><a href="{{url('')}}">Trang Chủ</a> </li>
                    <li><a href="{{url('')}}">Giới Thiệu</a> </li>
                    <div class="btn-group">
                    <a class=" dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Danh Mục
                        </a>
                        <ul class="dropdown-menu">
                        <li><a href="{{ url("product/tet_flower") }}">Hoa Tết 2023</a> </li>
                        <li><a href="{{ url("product/flower_wedding") }}">Hoa Cưới Đẹp</a> </li>
                        <li><a href="{{ url("product/flower_birthday") }}">Hoa Sinh Nhật</a> </li>
                        <li><a href="{{ url("product/flower_opeing") }}">Hoa Khai Trương</a> </li>
                        <li><a href="{{ url("/") }}">Tat Cả </a> </li>
                        </ul>
                    </div>
                    <li><a href="">Hoa Đặt</a></li>
                    <li><a href="{{ url('product/shoping-cart') }}">Giỏ hàng</a></li>
                
                
                </ul>
        
            </div>
            </div>
        </div>
        
    </div>
    </div>
</div>
