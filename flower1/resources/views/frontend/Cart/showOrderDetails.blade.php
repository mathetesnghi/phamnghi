@extends('frontend.layouts.app')
@section('content')
<div class="order">
    
        <div class="container3">
            
            <form action="{{ url("product/updateOrder") }}" method="POST">
                @csrf
                @method("PUT")                
                <div class="row">
                    
                    <div class="col">
                        
                        <h3 class="title">billing address</h3>
                        
                        <div class="inputBox">
                            
                            <span>Tên : {{ $orderData['full_name'] }}</span>
                            
                        </div>
                        <div class="inputBox">
                            
                            <span>Địa Chỉ: {{ $orderData['address'] }} </span>
                            
                        </div>
                        <div class="inputBox">
                            <span>Số Điện Thoại: {{ $orderData['telephone'] }}</span>
                            
                        </div>
                        <div class="inputBox">
                            <span>Email: {{ $orderData['email'] }}</span>
                            
                        </div>
                        
                    </div>
                    
                    
                    @if (Session::has('Cart') != null)
                    <div class="col">
                        <div class="cart-total">

                                
                                <p>
                                    
                                    <span>Số Lượng Sản Phẩm</span>
                                    
                                    <span>{{ Session::get('Cart')->totalQuantity }}</span>
                                    
                                </p>
                                <p>

                                    <span>Tổng: </span>

                                    <span>{{number_format(Session::get('Cart')->totalPrice) }}VNĐ  </span>

                                </p>

                                <p>

                                </p>

                            </div>
                            @endif
                            <!--
                                <div class="col">
        
                                    <h3 class="title">payment</h3>
                                    
                                    <div class="inputBox">
                            <span>cards accepted :</span>
                            <img src="images/card_img.png" alt="">
                        </div>
                        <div class="inputBox">
                            <span>name on card :</span>
                            <input type="text" placeholder="mr. john deo">
                        </div>
                        <div class="inputBox">
                            <span>credit card number :</span>
                            <input type="number" placeholder="1111-2222-3333-4444">
                        </div>
                        <div class="inputBox">
                            <span>exp month :</span>
                            <input type="text" placeholder="january">
                        </div>
        
                        <div class="flex">
                            <div class="inputBox">
                                <span>exp year :</span>
                                <input type="number" placeholder="2022">
                            </div>
                            <div class="inputBox">
                                <span>CVV :</span>
                                <input type="text" placeholder="1234">
                            </div>
                        </div>
                        
                    </div> -->
                </div>  
                    
                </div>
                
                <input type="submit" value="Thanh Toán" class="submit-btn">
                
            </form>
            
        </div>
    </div>
@endsection
