@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('register') }}" method="POST" >
            @csrf
            <div>
                <label for="user_name">Nhap Ten Nguoi Dung</label>
                <input type="text" name="user_name" id="user_name"  class="form-control @error('user_name') is-invalid @enderror">
                @error('user_name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="first_name">Nhap Ten</label>
                <input type="text" name="first_name" id="first_name" class="form-control @error('first_name') is-invalid @enderror">
                @error('first_name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="last_name">Nhap Ho</label>
                <input type="text" name="last_name" id="last_name" class="form-control @error('last_name') is-invalid @enderror">
                @error('last_name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="email">Nhap email</label>
                <input type="eamil" name="email" id="email" class="form-control @error('email') is-invalid @enderror">
                @error('email')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div>
                <label for="telephone">Nhap Số Điện Thoại</label>
                <input type="telephone" name="telephone" id="telephone" class="form-control @error('telephone') is-invalid @enderror">
                @error('telephone')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="password">Nhap mat khau</label>
                <input type="text" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="password">Nhap lai mat khau</label>
                <input type="text" name="password_confirmation" id="password" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

       
            <div class="form-check">
                <input class="form-check-input" id="role" name="role" type="checkbox" value="3" id="flexCheckChecked" checked ? checked :  @error('role') is-invalid @enderror >
                <label style="color:black" class="form-check-label"  for="flexCheckChecked">
                    Xac Nhan
                </label>
                @error('role')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            
            <div>
                <label for="address">Địa Chỉ</label>
                <input type="address" name="address" id="address" class="form-control @error('address') is-invalid @enderror">
                @error('address')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div>
                <label for="city">Thành Phố</label>
                <input type="city" name="city" id="city" class="form-control @error('city') is-invalid @enderror">
                @error('city')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div>
                <label for="country">Nước</label>
                <input type="country" name="country" id="country" class="form-control @error('country') is-invalid @enderror">
                @error('country')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
                  
    

            <button type="submit">Dang Ky</button>
        </form>
    </div>
@endsection
