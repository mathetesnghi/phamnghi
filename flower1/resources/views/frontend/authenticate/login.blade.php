<!DOCTYPE html>
<html>
<head>
	<title>Contact us</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/login.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
</head>
<body>
	<div class="container">
		<div class="contact-box">
			<div class="left">
                <img style="width:450px; hieght:300px" src="{{ asset("frontend/img/login/rose-1460773_960_720.png") }}" alt="">
            </div>
			<div class="right">
				<h2>Đăng Nhập   </h2>
                <form method="POST" action="/login">
                    @csrf
					<input type="text" name="email" class="field" placeholder="Email">
					<input type="text" name="password" class="field" placeholder="Password">
					<button type="sumit" class="btn">Đăng Nhập</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>