@extends('frontend.layouts.app')

@section('content')
<div class="container">


    <div class="row">
        <div class="col-md-5">
            @foreach ($products->img as $x => $image)
            
            {{-- @dd($products->img) --}}
                <img src="{{ asset('storage/backend/product/' . $image) }}" style="width:500px; height:500px" />
            @endforeach
        </div>
        <div id="text" class=" col-md-7">
            <h1>{{ $products->name }} <span style="font-size: 30px">"{{ $products->desc }}"-
                    {{ $products->category->name_category }}</span> </h1>

            <h2><i class="fa-solid fa-money-bill-1"></i>:{{ number_format($products->price) }}VN</h2>
            <p><i class="fa-solid fa-check"></i>Tặng MIỄN PHÍ thiệp, banner in (trị giá 20,000 vnđ).</p>
            <p><i class="fa-solid fa-check"></i>In logo/ảnh công ty/cá nhân lên banner</p>
            <p><i class="fa-solid fa-check"></i>Hỗ trợ Giao gấp trong 2 giờ</p>
            <p><i class="fa-solid fa-check"></i>Cần đặt hoa nhanh xin gọi:033578***</p>
            <p><i class="fa-solid fa-check"></i>Sản phẩm của chúng tôi luôn mang những ý nghĩa tốt đẹp nhất gửi tới
                người nhận.</p>
            <p><i>Lưu ý: Đây là sản phẩm hoa tươi tự nhiên nên có thể có chút khác biệt theo các mùa trong năm. Trường
                    hợp không đủ số lượng hoa như mẫu đã chọn, nhân viên tư vấn sẽ liên hệ với
                    Quý khách hàng để lựa chọn mẫu hoa thay thế phù hợp</i></p>

            <div class="row justify-content-start align-items-center p-0">
                <div class="col-md-3 p-0">
                    {{-- href="{{ url("product/{$products->id}/AddCart") }}" --}}
                    <a onclick="AddCart({{ $products->id }})" href="javascript:" class="btn btn-secondary">Thêm Vào Giỏ
                        Hàng</a>
                </div>

            </div>
        </div>
    </div>
</div>

    @include('frontend.layouts._footer')

    <script>
        function AddCart(id) {
            $.ajax({
                url: '/product/AddCart/' + id,
                type: 'GET',

            }).done(function(response) {
                // console.log(response); 
                RenderCart(response);
                alertify.success('Success message');
            });
        }
        $("#data-cart").on("click", ".si-close i", function() {
            $.ajax({
                url: '/product/Delete-Item-Cart/' + $(this).data("id"),
                type: 'GET',
            }).done(function(response) {
                RenderCart(response);
                alertify.success('Đã xóa sản phẩm');
            });
        });

        function RenderCart(response) {
            $("#data-cart").empty();
            $("#data-cart").html(response);
            $("#total-quantity-show").text($("#total-quantity-cart").val());
            // console.log($("#total-quantity-cart").val());
        }
    </script>
@endsection
