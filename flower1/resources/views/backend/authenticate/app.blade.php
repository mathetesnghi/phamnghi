<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- admin --}}
    <link rel="stylesheet" href="{{ asset('backend/css/reset.css')  }}">
    <link rel="stylesheet" href="{{asset('backend/css/app.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">

    {{-- Doarsh --}}
        <link rel="stylesheet" href="{{asset('backend/css/app.css') }}">
    <title>Document</title>
</head>
<body>
    @yield('form')
</body>
<script src="{{asset('backend/js/app.js')}}"></script>
</html>