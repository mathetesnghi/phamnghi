<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('backend/css/styles.css') }}" />
    <title>Admin</title>
</head>

<body>

    <div class="d-flex" id="wrapper">
        @include('backend.authenticate.layouts._nav')
        <div class="container-fluid px-4">
                @include('backend.authenticate.layouts._header')
                @yield('content')
        </div>
      
    </div>

    {{-- scrip off dash --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        var el = document.getElementById("wrapper");
        var toggleButton = document.getElementById("menu-toggle");

        toggleButton.onclick = function () {
            el.classList.toggle("toggled");
        };
    </script>

    <script src="{{asset('backend/js/ckeditor/ckeditor.js')}}" ></script>
    @stack('js')
</body>

</html>