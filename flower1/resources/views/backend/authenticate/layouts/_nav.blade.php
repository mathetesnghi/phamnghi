 
        <!-- Sidebar -->
        <div class="bg-white" id="sidebar-wrapper">
            <div class="sidebar-heading text-center py-4 primary-text fs-4 fw-bold text-uppercase border-bottom"><i
                    class="fas fa-user-secret me-2"></i>NO-FLower</div>
            <div class="list-group list-group-flush my-3">
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text active"><i
                        class="fas fa-tachometer-alt me-2"></i>Dashboard</a>

                {{-- <a href="{{ url('admin/account') }}" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-project-diagram me-2"></i>Account</a> --}}
        
                <li class="nav-item dropdown nav nav-item" >
                        <a class="nav-item dropdown-toggle list-group-item list-group-item-action bg-transparent second-text fw-bold " href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" ><i
                                class="fas fa-project-diagram me-2"></i>Account</a>
                          
                       
                        <ul  class="ms-3 dropdown-menu" aria-labelledby="navbarDropdownMenuLink ml">
                                <li><a class="dropdown-item" href="/admin/search/admin">Account Admin</a></li>
                                <li><a class="dropdown-item" href="/admin/search/client">Account Lient</a></li>
                        </ul>
                </li>
        
                <li class="nav-item dropdown nav nav-item" >
                        <a class="nav-item dropdown-toggle list-group-item list-group-item-action bg-transparent second-text fw-bold " href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" ><i
                                class="fas fa-chart-line me-2"></i>Products</a>
                          
                       
                        <ul  class="ms-3 dropdown-menu" aria-labelledby="navbarDropdownMenuLink ml">
                                <li><a class="dropdown-item" href="/admin/product/create">Create Flower</a></li>
                                <li><a class="dropdown-item" href="/admin/product/all">ALL</a></li>      
                                <li><a class="dropdown-item" href="/admin/product/tet_flower">Hoa Tet 2023</a></li>
                                <li><a class="dropdown-item" href="/admin/product/flower_wedding">Hoa Cuoi</a></li>
                                <li><a class="dropdown-item" href="/admin/product/flower_birthday">Hoa Sinh Nhat</a></li>
                                <li><a class="dropdown-item" href="/admin/product/flower_opeing ">Hoa Khai Chuong</a></li>
                        </ul>
                </li>
{{--                 
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-chart-line me-2"></i>Products</a> --}}
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-paperclip me-2"></i>Reports</a>
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-shopping-cart me-2"></i>Store Mng</a>
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-gift me-2"></i>Products</a>
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-comment-dots me-2"></i>Chat</a>
                <a href="#" class="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i
                        class="fas fa-map-marker-alt me-2"></i>Outlet</a>
                <a href="#" class="list-group-item list-group-item-action bg-transparent text-danger fw-bold"><i
                        class="fas fa-power-off me-2"></i>Logout</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
     