@extends('backend.authenticate.app')
@section('form')
<div id="wrapper">
    <form action="/admin" method="POST" id="form-login" >
        @csrf   
        <h1 class="form-heading">Admin</h1>
        <div class="form-group">
            <i class="far fa-user"></i>
            <input type="text" name="email" class="form-input" placeholder="Tên đăng nhập">
        </div>
        <div class="form-group">
            <i class="fas fa-key"></i>
            <input type="password" name="password" class="form-input" placeholder="Mật khẩu">
        </div>
        <button type="submit" class="form-submit">Đăng Nhập</button>
    </form>
</div>

@endsection