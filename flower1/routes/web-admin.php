<?php

use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\AuthenticateController;
use App\Http\Controllers\Admin\ProductController;
use Brick\Math\Exception\RoundingNecessaryException;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\RiskyTestError;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//amdin

    Route::get('/admin',[AuthenticateController::class,'showLoginAdmin']);
    
    Route::post('/admin',[AuthenticateController::class,'loginAdmin']);
    Route::get('/admin/login/index',[AuthenticateController::class,'index'])->middleware('must-admin');

    Route::get('logout1',[AuthenticateController::class,'logout']);
    // Route::get('admin/nav',[AuthenticateController::class,'showNav']);


//account
    Route::get('admin/search/admin',[AccountController::class,'adminInex']);
    Route::get('admin/search/client',[AccountController::class,'clientInex']);
    Route::get('admin/{id}/edit',[AccountController::class,'editAccount']);    
    Route::put('admin/{id}/',[AccountController::class,'update']);    
    Route::delete('admin/{id}',[AccountController::class,'delete']);

//product

   Route::get('admin/product/create',[ProductController::class,'create']);
   Route::post('admin/product/store',[ProductController::class,'store']);
   Route::put('admin/product/{id}',[ProductController::class,'update']);
   Route::get('admin/product/{id}/edit',[ProductController::class,'edit']);
//    Route::get('admin/product/flower_2023',[ProductController::class,'flower_2023']);

    Route::get('admin/product/all',[ProductController::class,'allProduct']);
    Route::get('admin/product/tet_flower',[ProductController::class,'tet_flower']);
    Route::get('admin/product/flower_wedding',[ProductController::class,'flower_wedding']);
    Route::get('admin/product/flower_birthday',[ProductController::class,'flower_birthday']);
    Route::get('admin/product/flower_opeing',[ProductController::class,'flower_opeing']);
    
    Route::delete('admin/delete/{id}',[ProductController::class,'destroy']);

