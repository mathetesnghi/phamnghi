<?php


use App\Http\Controllers\Web\AuthenticateController as WebAuthenticateController;
use App\Http\Controllers\Web\CartController;
use App\Http\Controllers\Web\CartCotroller;
use App\Http\Controllers\Web\ProductCotroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/flower', function () {
//     return view('frontend.layouts.app');
// });
Route::get('/', [WebAuthenticateController::class, 'showIndex']);
// ->middleware('must-auth')
Route::get('showlogin', [WebAuthenticateController::class, 'showlogin']);
Route::post('login', [WebAuthenticateController::class, 'login']);
Route::get('register', [WebAuthenticateController::class, 'showRegisterForm']);
Route::post('register',[WebAuthenticateController::class,'register']);
Route::get('logout',[WebAuthenticateController::class,'logout']);

//product
Route::get('product/{id}/details',[ProductCotroller::class,'detail_Product']);
Route::get('product/flower_wedding',[ProductCotroller::class,'flower_wedding']);
Route::get('product/tet_flower',[ProductCotroller::class,'tet_flower']);
Route::get('product/flower_birthday',[ProductCotroller::class,'flower_birthday']);
Route::get('product/flower_opeing',[ProductCotroller::class,'flower_opeing']);
//cart
Route::get('product/AddCart/{id}',[CartController::class,'AddCart']);
Route::get('product/Delete-Item-Cart/{id}',[CartController::class,'DeleteItemCart']);
Route::get('product/shoping-cart',[CartController::class,'showShoping']);
Route::get('product/Delete-List-Item-Cart/{id}',[CartController::class,'DeleteListItemCart']);
Route::get('product/Save-List-Item-Cart/{id}/{quantity}',[CartController::class,'SaveListItemCart']);

//order-shoping

Route::get('product/showOrderDetails',[CartController::class, 'showOrderDetails'])->middleware('must-auth');
Route::put('product/updateOrder',[CartController::class, 'updateOrder']);