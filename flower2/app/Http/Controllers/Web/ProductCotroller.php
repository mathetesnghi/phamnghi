<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\CommentReply;
use App\Models\Product;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProductCotroller extends Controller
{
    public function showProduct(Product $products, Request $request)
    {
        $price = $request->query('price');
        switch ($price) {
            case 1:
                $products = Product::where('price', '<', 1000000)->paginate(8);
                break;
            case 2:
                $products = Product::where('price', '>=', 1000000)
                    ->where('price', '<', 2000000)
                    ->paginate(8);
                break;
            case 3:
                $products = Product::where('price', '>=', 2000000)
                    ->where('price', '<', 3000000)
                    ->paginate(8);
                break;
            case 4:
                $products = Product::where('price', '>=', 3000000)->paginate(8);
                break;
            case 5:
                $products = Product::orderBy('created_at', 'desc')->paginate(12);
                break;
            case 6:
                $products = Product::orderBy('created_at', 'asc')->paginate(12);
                break;
            default:
                $products = Product::orderBy('id', 'desc')->paginate(8);
        }
        

        view('frontend.layouts._product_feature', compact('products'));
    }
    public function detail_Product($id)
    {
        // dd($id);
        // $products= Product::all();
        // $products = DB::table('products')->where('id',$id)->get();
        // dd($products);
        $products = Product::with('comment')->findOrFail($id);
        // $comments= Comment::with('user')->orderBy('created_at','desc')->get()->toArray();
        $comments = Comment::with(['user', 'replies' => function ($query) {
            $query->with(['user', 'replies']);
        }])->where('product_id', $id)->whereNull('parent_id')->orderBy('created_at', 'desc')->get();
        $avatar = User::where('id', Auth::user()->id)->get()->toArray();
        $relatedProducts = Product::where('category_id', $products->category_id)
            ->where('id', '!=', $id)
            ->take(8)
            ->get();
        // dd($relatedProducts);
        // @dd($avatar);
        // $products = Product::orderBy('id','desc')->paginate(8);
        // $products= Product::with('comment')->with('product');
        $products->views++;
        $products->save();


        //category
        $categories = Category::all();
        return view('frontend.product.details', compact('products', 'comments', 'avatar', 'relatedProducts', 'categories'));
    }
    // public function addCart($id)
    // {
    //     $products= Product::findOrFail($id);
    //     $cart = session()->get('cart', []);

    //     if(isset($cart[$id])) {
    //         $cart[$id]['quantity']++;
    //     }  else 
    //     {
    //         $cart[$id]= [
    //             'name' => $products->name,
    //             'img'=>$products->img,
    //             'price' => $products->price,
    //             'quantity' => 1
    //         ];
    //     }

    //     session()->put('cart', $cart);
    //     return redirect()->back()->with('success', 'Product add to cart successfully!');
    // }

    public function searchAll(Request $request)
    {
        // dd($request->input());
        $searchText = $request->query('search');
        $products = $searchText ? Product::with('category')->with('inventory')
            ->where('name', 'LIKE', "%$searchText%")->paginate(8) : Product::with('category')->with('inventory')->paginate(10);
        $products->appends(['search' => $searchText]);
        $popularProducts = Product::orderByDesc('views')->take(8)->get();
        $categories = Category::all();
        return view('frontend.index', compact('products', 'popularProducts', 'categories'));
    }
    // public function flower_wedding(Product $products)
    // {


    //     $products=Product::whereHas('category', function ($query) {
    //         return $query->where('desc', 'flower-wedding');
    //     })->limit(8)->with('inventory')->orderBy('id','desc')->paginate(8);

    //     $popularProducts = Product::orderByDesc('views')->take(8)->get();
    //     $productWithCategories= Product::with('category')->get();

    //     return view('frontend.index', compact('products','productWithCategories','popularProducts' ));
    // }
    // public function tet_flower(Request $request)
    // {

    //         $products=Product::whereHas('category', function ($query) {
    //             return $query->where('name_category', 'Hoa Tet 2023');
    //         })->limit(16)->with('inventory')->orderBy('id','desc')->paginate(8);
    //         $popularProducts = Product::orderByDesc('views')->take(8)->get();


    //     return view('frontend.index',compact('products','popularProducts'));

    // }
    // public function flower_birthday()
    // {
    //     $products=Product::whereHas('category', function ($query) {
    //         return $query->where('desc', 'flower-birthday');
    //     })->limit(16)->with('inventory')->orderBy('id','desc')->paginate(8);
    //     $popularProducts = Product::orderByDesc('views')->take(8)->get();

    //     return view('frontend.index',compact('products','popularProducts'));
    // }
    // public function flower_opeing()
    // {
    //     $products=Product::whereHas('category', function ($query) {
    //         return $query->where('desc', 'flower-opeing');
    //     })->limit(16)->with('inventory')->orderBy('id','desc')->paginate(8);
    //     $popularProducts = Product::orderByDesc('views')->take(8)->get();

    //     return view('frontend.index',compact('products','popularProducts'));
    // }

    public function orderdetail()
    {
        $name = 'Phạm Năng Nghi';
        Mail::send('frontend.email.test', compact('name'), function ($email) use ($name) {
            $email->subject('demo');
            $email->to('mathetesnghi@gmail.com', $name);
        });
    }


    // public function store(Request $request, $id)
    // {
    //     $product = Product::findOrFail($id);
    //     $commentData = [
    //         'user_id' => Auth::user()->id,
    //         'product_id' =>$product->id,
    //         'content'=> $request->input('content'),
    //         'parent_id' => $request->input('parent_id')
    //     ];
    //     Comment::create($commentData);
    //     return redirect()->back();
    // }
    public function store(Request $request, $id)
    {
        $request->validate([
            'user_name' => 'content',

        ]);

        $product = Product::findOrFail($id);

        $parent_id = $request->input('parent_id');
        // $level = $parent_id ? Comment::find($parent_id)->level + 1 : 1;

        $commentData = [
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
            'content' => $request->input('content'),
            'parent_id' => $parent_id,
            // 'level' => $level
        ];
        Comment::create($commentData);

        return redirect()->back();
    }
    public function store2(Request $request, $id)
    {
        $request->validate([
            'user_name' => 'content',
        ]);
        $product = Product::findOrFail($id);
        $commentData = [
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
            'content' => $request->input('content'),
            'parent_id' => $request->input('parent_id')
        ];
        Comment::create($commentData);
        return redirect()->back();
    }
    // public function store(Request $request, $id)
    // {
    //     $product = Product::findOrFail($id);
    //     // dd($product);

    //     $commentData = [
    //         'user_id' => Auth::user()->id,
    //         'product_id' => $product->id,
    //         'content' => $request->input('content'),
    //     ];

    //     if ($request->has('parent_id')) {
    //         // If the request has parent_id, it means this is a comment reply
    //         $commentData['parent_id'] = $request->input('parent_id');
    //         CommentReply::create($commentData);
    //     } else {
    //         // Otherwise, it's a new comment
    //         Comment::create($commentData);
    //     }

    //     return redirect()->back();
    // }

}
