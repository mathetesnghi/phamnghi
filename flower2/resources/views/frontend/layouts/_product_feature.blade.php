@extends('frontend.layouts.app')
@section('content')
    @include('frontend.layouts._slide')
    <div class="container">

        {{-- <div class="row"> --}}
        @include('frontend.layouts._filter_left')
        {{-- @include('frontend.layouts._filter_right') --}}


        {{-- </div> --}}
    </div>
    {{-- @include('frontend.layouts._filter_left')
        @include('frontend.layouts._filter_right') --}}
    <div class="container1">
        {{-- @foreach ($productWithCategories as $product)

    <h3>{{ $product->category->name_category }}</h3> 

@endforeach --}}
        {{-- @php
            $previousCategory = null;
        @endphp
        @foreach ($products as $x => $product)
   
            @if ($product->category)
                <h3 style="text-align:center">{{ $product->category['name_category'] }}</h3>
            @else
                <h3 style="text-align:center">Sản Phẩm</h3>
            @endif
        @endforeach --}}
{{-- 
        @php
            $previousCategory = null;
        @endphp

        @foreach ($productWithCategories as $productWithCategorie)
            @if ($productWithCategorie->category)
                @if ($previousCategory != $productWithCategorie->category->name_category)
                    <h3 style="text-align:center">{{ $productWithCategorie->category->name_category }}</h3>
                    @php
                        $previousCategory = $productWithCategorie->category->name_category;
                    @endphp
                @endif
                @if (
                    $previousCategory == 'hoa cưới' &&
                        $product->category->name_category == 'hoa cưới' &&
                        $product != $products->first())
                    @continue
                @endif
            @else --}}
                <h3 style="text-align:center">Sản Phẩm</h3>
            {{-- @endif
        @endforeach --}}

        <div class="products-container">
            <div class="row">
                @foreach ($products as $product)
                    <div class="col-md-3">
                        <a href="{{ url("product/{$product->id}/details") }}">
                            <div class="product" data-name="p-1">
                                @foreach ($product->img as $images)
                                    {{-- @dd($product) --}}
                                    <img src="{{ asset('storage/backend/product/' . $images) }}"
                                        style="width:180px; height:180px" />
                                @endforeach
                                <h3>{{ $product->name }}</h3>
                                <div class="price">{{ number_format($product->price) }} <span>VN</span></div>
                            </div>
                        </a>
                    </div>
                @endforeach
                {{ $products->links() }}
            </div>
        </div>
    </div>

    <div class="container1">
        <h3 style="text-align:center">Sản Phẩm Nổi Bật</h3>
        <div class="products-container">
            <div class="row">
                @foreach ($popularProducts as $popularProduct)
                    <div class="col-md-3">
                        <a href="{{ url("product/{$popularProduct->id}/details") }}">
                            <div class="product" data-name="p-1">
                                @foreach ($popularProduct->img as $images)
                                    {{-- @dd($product) --}}
                                    <img src="{{ asset('storage/backend/product/' . $images) }}"
                                        style="width:180px; height:180px" />
                                @endforeach
                                <h3>{{ $popularProduct->name }}</h3>
                                <div class="price">{{ number_format($popularProduct->price) }} <span>VN</span></div>
                            </div>
                        </a>
                    </div>
                @endforeach
                {{-- {{ $popularProducts->links() }} --}}
            </div>
        </div>

    </div>
    @include('frontend.layouts._footer')
@endsection
