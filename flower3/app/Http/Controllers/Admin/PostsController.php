<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryPost;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function showCreatePost()
    {
        $categories = CategoryPost::all();
        // dd($categories);
        return view('backend.posts.ShowCreatePosts', compact('categories'));
    }
    public function store(Request $request)
    { 
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'category_id' => 'required|exists:categoriesPosts,id',
            'content' => 'required',
            // 'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            'images'=> 'required'
        ]);
    
        // Create new post
        $post = new Posts();
        $post->title = $validatedData['title'];
        $post->content = $validatedData['content'];
        $post->category_id = $validatedData['category_id'];
        $post->user_id = Auth::user()->id;
        $post->published_at = now();
        $post->save();
    
        // Save images to disk and database
        $images = [];
        if ($request->hasFile('images')) {
            $images = $request->file('images');
            $imageNames = [];
            foreach ($images as $image) {
                // store image file and get the file name
                // $imageName = $image->store('backend/posts', 'public');
                // $imageName = $image->store('backend/posts', 'public');
                $imageName= $image->storeAs('', $image->getClientOriginalName(), 'posts');
                $imageNames[] = $imageName;
            }
            // save the image names as a JSON array in the 'images' column
            $post->images = json_encode($imageNames);
            $post->save();
        }
        return redirect('/admin/posts/showCreatePost')->with('success', 'Post created successfully.');
    
      
    }
}
