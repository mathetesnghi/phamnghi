@extends('frontend.posts.layouts.app')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div  class="card-header"><b style="font-size: 23px" >{{ $posts->title }}</b></div>
                    @foreach (json_decode($posts->images) as $key => $image)
                        @if ($key == 0)
                            <img src="{{ asset('storage/backend/posts/' . $image) }}" width="350px" height="350px" alt=""class="post-img">
                            {{-- <div style="background-image: url('{{ asset('storage/backend/posts/' . $image) }}'); filter: blur(10px);" class="post-img"></div> --}}
                        @endif
                    @endforeach
                    <div class="card-body">
                        {!! $posts->content !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Author</div>

                    <div class="card-body">
                        <div class="media">
                            <img src="{{ asset('storage/frontend/userProfile/' . $posts->user->avatar) }}" width="200px" height="200px" class="mr-3"
                                alt="Author Image">
                            <div class="media-body">
                                <h5 class="mt-0">{{ $posts->user->user_name }}</h5>
                                <p>{{ $posts->user->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
